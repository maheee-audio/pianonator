# Pianonator

Shows Scales and Chords on a Piano Keyboard

https://maheee-audio.gitlab.io/pianonator/

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build and Deployment

```
ng build --prod --base-href "https://maheee.github.io/pianonator/"
angular-cli-ghpages
```
