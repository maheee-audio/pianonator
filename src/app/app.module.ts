import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { ScaleService } from './services/scale.service';
import { ChordService } from './services/chord.service';

import { AppComponent } from './app.component';
import { ScaleChordComponent } from './components/scalechord.component';
import { IderComponent } from './components/ider.component';
import { KeyComponent } from './components/keyboard/key.component';
import { KeyboardComponent } from './components/keyboard/keyboard.component';
import { StaffComponent } from './components/staff/staff.component';
import { NoteComponent } from './components/staff/note.component';



@NgModule({
  declarations: [
    AppComponent,
    KeyComponent,
    KeyboardComponent,
    ScaleChordComponent,
    StaffComponent,
    NoteComponent,
    IderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgSelectModule
  ],
  providers: [
    ScaleService,
    ChordService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
