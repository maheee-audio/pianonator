
export interface Scale {
  name: string,
  pattern: number[]
}

export interface NoteName {
  no: number,
  name: string,
  sign: string
}
