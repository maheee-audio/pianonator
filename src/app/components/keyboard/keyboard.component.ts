import { Component, EventEmitter, Output, Input } from '@angular/core';
import { KeyStyle } from './key.component';


@Component({
  selector: 'keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent {

  keys = [
    {style: KeyStyle.WHITE, key:  0 }, {style: KeyStyle.BLACK, key:  1 },
    {style: KeyStyle.WHITE, key:  2 }, {style: KeyStyle.BLACK, key:  3 },
    {style: KeyStyle.WHITE, key:  4 },
    {style: KeyStyle.WHITE, key:  5 }, {style: KeyStyle.BLACK, key:  6 },
    {style: KeyStyle.WHITE, key:  7 }, {style: KeyStyle.BLACK, key:  8 },
    {style: KeyStyle.WHITE, key:  9 }, {style: KeyStyle.BLACK, key: 10 },
    {style: KeyStyle.WHITE, key: 11 },
  ]

  @Input() start:number;
  @Input() specialKey:number;
  @Input() highlight1Keys:number[];
  @Input() highlight2Keys:number[];
  @Input() highlight3Keys:number[];

  @Output() keyDown:EventEmitter<number> = new EventEmitter<number>();
  @Output() keyUp:EventEmitter<number> = new EventEmitter<number>();
  @Output() keyClick:EventEmitter<number> = new EventEmitter<number>();

  onKeyDown(key:number) {
    this.keyDown.emit(key);
    this.keyClick.emit(key);
  }

  onKeyUp(key:number) {
    this.keyUp.emit(key);
  }

}
