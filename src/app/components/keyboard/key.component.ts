import { Component, Input } from '@angular/core';


export enum KeyStyle {
  BLACK = 'BLACK',
  WHITE = 'WHITE'
}

@Component({
  selector: 'key',
  templateUrl: './key.component.html',
  styleUrls: ['./key.component.scss']
})
export class KeyComponent {

  @Input() style:KeyStyle = KeyStyle.BLACK;
  @Input() special:boolean = false;
  @Input() highlight1:boolean = false;
  @Input() highlight2:boolean = false;
  @Input() highlight3:boolean = false;

}
