import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'ider',
  templateUrl: './ider.component.html',
  styleUrls: ['./ider.component.scss']
})
export class IderComponent implements OnInit {

  notes:number[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  private toggleNumberInArray(array:number[], n:number) {
    let index:number = array.indexOf(n);
    if (index > -1) {
      array.splice(index, 1);
    } else {
      array.push(n);
    }
    array.sort((a, b) => {return a == b ? 0 : (a < b ? -1 : 1);});
  }

  staffClick(number) {
    console.log(number);
    this.toggleNumberInArray(this.notes, number);
  }

}
