import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import { ScaleService } from '../services/scale.service';
import { ChordService } from '../services/chord.service';
import { Scale } from '../model';
import { scales } from '../data';


@Component({
  selector: 'scale-chord',
  templateUrl: './scalechord.component.html',
  styleUrls: ['./scalechord.component.scss']
})
export class ScaleChordComponent implements OnInit {

  scales = scales;

  selectedStart$:Observable<number>;
  selectedScale$:Observable<Scale>;
  scale$:Observable<number[]>;
  scale2$:Observable<number[]>;
  chords$:Observable<number[][]>;

  constructor(private scaleService:ScaleService, private chordService:ChordService) {
    this.selectedStart$ = scaleService.selectedStart$;
    this.selectedScale$ = scaleService.selectedScale$;

    this.scale$ = scaleService.scale$;
    this.scale2$ = scaleService.scale2$;
    this.chords$ = chordService.chords$;
  }

  ngOnInit(): void {
    this.scale$.subscribe(scale => {
      this.chordService.setScale(scale);
    });

    this.scaleService.setScale(scales[0]);
  }
  
  onKeyClick(key) {
    this.scaleService.setStart(key);
  }

  onScaleSelect(scale) {
    this.scaleService.setScale(scale);
  }

  getChordName(i, chord) {
    return this.chordService.getChordName(i, chord);
  }

  getChordDescription(i, chord) {
    return this.chordService.getChordDescription(i, chord);
  }

}
