import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';


export enum Accidental {
  NONE,
  SHARP,
  FLAT,
  NATURAL
}

export enum Type {
  WHOLE,
  HALF,
  QUARTER,
  EIGHTH,
  SIXTEENTH,
  THIRTYSECOND,
  SIXTYFOURTH,
  HUNDREDTWENTYEIGHTH
}

@Component({
  selector: 'note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent {

  @Input() lineNo:number;
  @Input() horizontalPosition:number;
  @Input() accidental:Accidental = Accidental.NONE;
  @Input() type:Type = Type.WHOLE;
  @Input() isPause:boolean = false;

  @Output() noteClick:EventEmitter<void> = new EventEmitter<void>();

  Accidental = Accidental;
  Type = Type;

  onNoteClick() {
    this.noteClick.emit();
  }
}
