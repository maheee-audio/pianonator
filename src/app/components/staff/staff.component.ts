import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Accidental, Type } from './note.component';


export enum Clef {
  TREBLE,
  FRENCH_VIOLIN,

  ALTO,
  TENOR,
  BARITONE_C,
  MEZZO_SOPRANO,
  SOPRANO,

  BASS,
  BARITONE,
  SUB_BASS
}

@Component({
  selector: 'staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {

  @Input() helpersTop:number = 3;
  @Input() helpersBottom:number = 3;
  @Input() clef:Clef = Clef.TREBLE;
  //@Input() notes:number[] = [];

  @Output() staffClick:EventEmitter<number> = new EventEmitter<number>();

  Accidental = Accidental;
  Type = Type;
  Clef = Clef;

  linesArray:number[];
  helpersTopArray:number[];
  helpersBottomArray:number[];

  constructor() {
  }

  ngOnInit(): void {
    this.linesArray = new Array(5);
    this.helpersTopArray = new Array(this.helpersTop);
    this.helpersBottomArray = new Array(this.helpersBottom);
  }
/*
  showNote(number:number) {
    return this.notes.indexOf(number) > -1;
  }
*/
  onStaffClick(number:number) {
    this.staffClick.emit(number);
  }

}
