import { Scale, NoteName } from "./model";


export let scales:Scale[] = [
  { name: 'none', pattern: [] }, //TTSTTTS
  { name: 'Major', pattern: [2,2,1,2,2,2,1] }, //TTSTTTS
  { name: 'Minor', pattern: [2,1,2,2,1,2,2] }, //TSTTSTT
  { name: 'Minor (harmonic)', pattern: [2,1,2,2,1,3,2] }, //TSTTSTT
  { name: 'Major Blues', pattern: [2,1,1,3,2,3] },
  { name: 'Minor Blues', pattern: [3,2,1,1,3,2] },
  { name: 'Minor Pentatonic', pattern: [3,2,2,3,2] },
  { name: 'Major Pentatonic', pattern: [2,2,3,2,3] }
]

export let notes:NoteName[] = [
  { no:  0, name: 'C',  sign: 'C'  },
  { no:  1, name: 'CS', sign: 'C♯' },
  { no:  2, name: 'D',  sign: 'D'  },
  { no:  3, name: 'DS', sign: 'D♯' },
  { no:  4, name: 'E',  sign: 'E'  },
  { no:  5, name: 'F',  sign: 'F'  },
  { no:  6, name: 'FS', sign: 'F♯' },
  { no:  7, name: 'G',  sign: 'G'  },
  { no:  8, name: 'GS', sign: 'G♯' },
  { no:  9, name: 'A',  sign: 'A'  },
  { no: 10, name: 'AS', sign: 'A♯' },
  { no: 11, name: 'B',  sign: 'B'  }
]