import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { map, tap } from 'rxjs/operators';
import { notes } from '../data';


@Injectable()
export class ChordService {

  private selectedScale:BehaviorSubject<number[]> = new BehaviorSubject<number[]>(null);

  selectedScale$:Observable<number[]> = this.selectedScale.asObservable();

  chords$:Observable<number[][]> = this.selectedScale$.pipe(
    map(scale => {
      let res = [];
      let chord;

      let getNoteFromScale = p => {
        let a = 0;
        while (p >= scale.length) {
          p -= scale.length;
          a += 12;
        }
        return scale[p] + a;
      };

      for (let i = 0; i < scale.length; ++i) {
        chord = []
        chord.push(getNoteFromScale(i + 0));
        chord.push(getNoteFromScale(i + 2));
        chord.push(getNoteFromScale(i + 4));
        
        chord.push(getNoteFromScale(i + 6));
        chord.push(getNoteFromScale(i + 8));
        
        res.push(chord);
      }

      return res;
    })
  );

  setScale(scale:number[]) {
    this.selectedScale.next(scale);
  }

  getChordName(i:number, chord:number[]) {
    let firstThird = chord[1] - chord[0];
    let secondThird = chord[2] - chord[1];
    
    let name = notes[chord[0] % 12].sign;
    name += ' - ';

    if (firstThird > 3) {
      name += ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII'][i];
    } else {
      name += ['i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii'][i];
      if (secondThird <= 3) {
        name += String.fromCharCode(0x2070);
      }
    }

    return name;
  }

  getChordDescription(i:number, chord:number[]) {
    let name = '( ';

    for (let note of chord) {
      name += notes[note % 12].sign + ' ';
    }

    name += ')';

    return name;
  }

}