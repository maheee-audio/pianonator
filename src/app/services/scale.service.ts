import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { map } from 'rxjs/operators';

import { Scale } from "../model";


@Injectable()
export class ScaleService {

  private selectedStart:BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private selectedScale:BehaviorSubject<Scale> = new BehaviorSubject<Scale>(null);

  selectedStart$:Observable<number> = this.selectedStart.asObservable();
  selectedScale$:Observable<Scale> = this.selectedScale.asObservable();

  scale$:Observable<number[]> = combineLatest(this.selectedStart$, this.selectedScale$).pipe(
    map(args => {
      let start:number = args[0];
      let scale:Scale = args[1];

      if (!scale || !scale.pattern || !scale.pattern.length) {
        return [];
      }

      let res = [];
      let p = start;

      for (let j of scale.pattern) {
        res.push(p);
        p += j;
        //p %= 12;
      }

      return res;
    })
  );

  scale2$:Observable<number[]> = this.scale$.pipe(map(scale => {
    for (let i = 0; i < scale.length; ++i) {
      scale[i] += 12;
    }
    return scale;
  }));

  setStart(start:number) {
    this.selectedStart.next(start);
  }

  setScale(scale:Scale) {
    this.selectedScale.next(scale);
  }

}